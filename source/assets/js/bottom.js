/**
 *= require jquery/dist/jquery
 *= require bootstrap
 *= require wow/dist/wow
 *= require PACE/pace
 *= require jquery.easing/js/jquery.easing
 *= require jQuery.mmenu/dist/core/js/jquery.mmenu.min.all
 *= require _hamburger
 */

$(function(){

    // Init WOW.js
    new WOW().init();

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    // http://startbootstrap.com/template-overviews/creative/
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // MMenu
    $("#my-menu").mmenu({
       "offCanvas": {
           "position": "right"
        },
        "navbars": [
            {
                "position": "bottom",
                "content": [
                    "<a class='mdi mdi-rss' href='http://blog.linuxmint.com/?feed=rss2'><span class='hidden'>Feed</span></a>",
                    "<a class='mdi mdi-facebook' href='//www.facebook.com/pages/Linux-Mint/185238108180410' target='_blank'><span class='hidden'>Facebook</span></a>",
                    "<a class='mdi mdi-twitter' href='http://twitter.com/linux_mint'><span class='hidden'>Twitter</span></a>"
                ]
            }
        ]
    });

    $('#my-menu').data('mmenu').bind('closing', function () {
        $('#lm-menu-button button').removeClass('is-active');
    });

    // Download
    $('#lm-download-laptop').carousel({
        interval: false
    });

    $('#lm-download-editions button').on('click', function(e){

    });

});
